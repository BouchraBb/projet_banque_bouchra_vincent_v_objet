package gestionBanque.dao;
import gestionBanque.dto.*;
import gestionBanque.PrincipalBanque;
import java.util.*;


public class CompteBancaireDao {
	

	private static CompteBancaireDao instance;
	
	public static CompteBancaireDao getInstance() {
		if(instance==null)
			instance = new CompteBancaireDao();
		
		return instance;
	}
	
	private static int indexCompteBancaire = 1;
	
	public boolean existWithNumero(CompteBancaire[] tab, String numero){
		boolean exist= false;
		for(int i=0; i<tab.length; i++){
				if(tab[i].getNumeroCompte().equals(numero)){
					exist= true;
				}
			}
			return exist;
	}
	
	public CompteBancaire[]creerCompte1(CompteBancaire[] comptesBancaires){  
		CompteBancaire compte1= new CompteBancaire(999,"XX111111","CC",25950.55, true);
		comptesBancaires[0]=compte1;
		return comptesBancaires;
	}
	
	public CompteBancaire[] redimCompteBancaires(CompteBancaire[] tab ){	
		CompteBancaire[] tmp = new CompteBancaire[tab.length + 1 ];      
		for(int i=0; i<tmp.length-1;i++){
				tmp[i] = tab[i];                                          
			System.out.println();
		}                                                                        
		return tmp;
	}
	
	public CompteBancaire[] save(CompteBancaire compteBancaire, CompteBancaire[] comptesBancaires){
		comptesBancaires= redimCompteBancaires(comptesBancaires);
		comptesBancaires[indexCompteBancaire]= compteBancaire;
		indexCompteBancaire++;
		return comptesBancaires;		
	}

	public  CompteBancaire[] modifierCompteBancaire(CompteBancaire[] comptesBancaires, int indexCompteAModifier,String newInfo ){
		if(PrincipalBanque.choixSeq.equals("1")){
			double solde = Double.parseDouble(newInfo);
			if(comptesBancaires[indexCompteAModifier].getType().equals("CC")){
				solde -= 25 ;
			}
			else if(comptesBancaires[indexCompteAModifier].getType().equals("LA")){
				solde-=25+(0.1*solde);	
			}
			else{
				solde-=25+(0.25*solde);
			}
			comptesBancaires[indexCompteAModifier].setSolde(solde) ;
		}else if(PrincipalBanque.choixSeq.equals("2")){
			if(comptesBancaires[indexCompteAModifier].getDecouvertEstAutorise()==true){
				comptesBancaires[indexCompteAModifier].setDecouvertEstAutorise(false);	
			}
			else{
				comptesBancaires[indexCompteAModifier].setDecouvertEstAutorise(true);
			}
		}
		return comptesBancaires;
	}
	
	public static CompteBancaire[] supprimerCompteBancaire(CompteBancaire[] comptesBancaires, int indexCompte){
		CompteBancaire[] newTab = new CompteBancaire[comptesBancaires.length-1];
		for(int i=0; i<newTab.length;i++){
				if(i <indexCompte){
					newTab[i] = comptesBancaires[i]; 
				} else {
					newTab[i] = comptesBancaires[i+1];
						}
				}
			System.out.println("le compte bancaire numero "+ comptesBancaires[indexCompte].getNumeroCompte()+ " a bien ete supprime");				
			indexCompteBancaire-- ;
		return newTab;
	}
	
	public static CompteBancaire[] supprimerComptesClient(CompteBancaire[] comptesBancaires, String id){
		int indexCompteASpp;
		for (int i=0 ; i<comptesBancaires.length; i++){		
				if(id.equals(comptesBancaires[i].getIdClient())==true){ 
					indexCompteASpp=i;
					comptesBancaires=supprimerCompteBancaire(comptesBancaires,indexCompteASpp);  
					i--;
				}
			}			
		return comptesBancaires;
	}
	
	public CompteBancaire chercherCompteParNumero(CompteBancaire[]comptesBancaires, String numeroCompte){
		CompteBancaire compte= null;
		for(int i=0; i<comptesBancaires.length; i++){
			if(comptesBancaires[i].getNumeroCompte().equals(numeroCompte)==true)
				compte = comptesBancaires[i];
		}
		return compte;
	}
	public CompteBancaire[] chercherComptesClient(CompteBancaire[] comptesBancaires, String id){
		CompteBancaire[] comptesClient= new CompteBancaire[1];
		int index=0;
		int cpt=0;
		for (int i=0 ; i<comptesBancaires.length; i++){		
			if(id.equals(comptesBancaires[i].getIdClient())==true){
				cpt++;
				if(cpt>comptesClient.length){
				comptesClient= redimCompteBancaires(comptesClient);
				}				
				comptesClient[index]= comptesBancaires[i];
				index++;
				
			}
		}
	return comptesClient;
	}			
}
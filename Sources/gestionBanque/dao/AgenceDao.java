package gestionBanque.dao;
import gestionBanque.dto.Agence;
import gestionBanque.dto.CompteBancaire;
import gestionBanque.PrincipalBanque;
import java.util.*;


public class AgenceDao {
	

	private static AgenceDao instance;
	
	public static AgenceDao getInstance() {
		if(instance==null)
			instance = new AgenceDao();
		
		return instance;
	}
	
	Scanner sc = new Scanner(System.in);
	static Random run = new Random();
	static int agenceSup;
	static int dernierCodeAgence = 99;
	static int indexAgence = 1;
	
	public boolean existWithCode(Agence[] tab, int code){
		boolean exist= false;
		for(int i=0; i<tab.length; i++){
				if(tab[i].getCode().equals(code)){
					exist= true;
				}
			}
			return exist;
	}
	public Agence[] redimAgences(Agence[] tab ){	
		Agence[] tmp = new Agence[tab.length + 1 ];      
		for(int i=0; i<tmp.length-1;i++){
				tmp[i] = tab[i];                                          
			System.out.println();
		}                                                                        
		return tmp;
	}
	public Agence[] creerAgenceParDefaut(Agence[] agences){
		Agence agenceGenerique= new Agence(999,"generique","inconnu");
		agences[0]= agenceGenerique;
		return agences;
	}
	
	public Agence genererCode( Agence agence){ 
		agence.setCode(dernierCodeAgence+1);
		return agence;
	}
	public Agence[] save(Agence agence, Agence[] agences){
		agences= redimAgences(agences);
		agence= genererCode(agence);
		agences[indexAgence]= agence;
		indexAgence++;
		dernierCodeAgence= agence.getCode();
		return agences;		
	}

	public  Agence[] modifierAgence(Agence[] agences, int indexAgenceAModifier,String newInfo ){
			if(PrincipalBanque.choixSeq.equals("1")){
					agences[indexAgenceAModifier].setNom(newInfo);
			}else if(PrincipalBanque.choixSeq.equals("2")){
					agences[indexAgenceAModifier].setAdresse(newInfo);
			}
		return agences;
	}
	
	public  Agence[] supprimerAgence(Agence[] agences, int indexAgence){
		Agence[] newTab = new Agence[agences.length-1];
		for(int i=0; i<newTab.length;i++){
				if(i <indexAgence){
					newTab[i] = agences[i]; 
				} else {
					newTab[i] = agences[i+1];
						}
				}	
			agenceSup = agences[indexAgence].getCode();															
			indexAgence-- ;
		return newTab;
	}
	public CompteBancaire[] migration(Agence[] agences, CompteBancaire[] comptesBancaires){
					
			for(int i=0; i<comptesBancaires.length;i++){
					if(comptesBancaires[i].getCodeAgence()==agenceSup){
						comptesBancaires[i].setCodeAgence(999);
						System.out.print("le compte " +comptesBancaires[i].getNumeroCompte()+" a bien migre vers l'agence generique ");
					}
				System.out.println();	
			}
		return comptesBancaires;
	}
					
}
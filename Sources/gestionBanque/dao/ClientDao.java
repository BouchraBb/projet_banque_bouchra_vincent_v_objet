package gestionBanque.dao;
import gestionBanque.dto.Client;
import gestionBanque.dto.CompteBancaire;
import gestionBanque.PrincipalBanque;
import java.util.*;

public class ClientDao{
	
	Scanner sc = new Scanner(System.in);
	static int indexClient = 1;
	private static ClientDao instance;
	
	public static ClientDao getInstance() {
	if(instance==null)
		instance = new ClientDao();
	
	return instance;
	}
	
	public boolean existWithId(Client[]tab,  String id){
		boolean exist= false;
		for(int i=0; i<tab.length; i++){
				if(tab[i].getId().equals(id)){
					exist= true;
				}
			}
			return exist;
	}
	
	public Client[] redimClients(Client[] tab ){	
	Client[] tmp = new Client[tab.length + 1 ];      
	for(int i=0; i<tmp.length-1;i++){
			tmp[i] = tab[i];                                          
		System.out.println();
	}                                                                        
	return tmp;
	}
	
	public Client[] creerClient1(Client[] clients){
		Client client1= new Client("DUPONT","Nina","15/03/1993","nina-dupont@gmail.com");
		clients[0]= client1;
		
		return clients;
	}
	
	public Client[] save(Client client, Client[] clients){
	clients= redimClients(clients); 
	clients[indexClient]= client;
	indexClient++;
	
	return clients;		
	}
	
	public Client[] supprimerClient(int indexClien, Client[] clients){
		Client[] newTab = new Client[clients.length-1];
		for(int i=0; i<newTab.length; i++){
			if(i<indexClien){
				newTab[i] = clients[i];
			} else{newTab[i] = clients[i+1];}
		}
		indexClient--;
		return newTab;
	}
	
	public Client[] modifierClient(Client[] clients, String nouvelleElement, int indexModif, int indexClientAModifier){
		if(indexModif == 1){
			clients[indexClientAModifier].setNom(nouvelleElement); 
		} else if(indexModif == 2){
			clients[indexClientAModifier].setPrenom(nouvelleElement); 
		} else if(indexModif == 3){
			clients[indexClientAModifier].setDateDeNaissance(nouvelleElement); 
		} else if(indexModif == 4){
			clients[indexClientAModifier].setEmail(nouvelleElement); 
		}
		
		return clients;
	}
	
	public void rechercherElementClient(Client[] clients, CompteBancaire[] comptesBancaires, int choix, int indexClientAAfficher){

		if(choix == 1){
			System.out.println(clients[indexClientAAfficher].getNom());
			System.out.println(clients[indexClientAAfficher].getPrenom());
			System.out.println(clients[indexClientAAfficher].getDateDeNaissance());
			System.out.println(clients[indexClientAAfficher].getEmail());
		} else if(choix == 2){
			System.out.println(clients[indexClientAAfficher].getNom());
			System.out.println(clients[indexClientAAfficher].getPrenom());
			System.out.println(clients[indexClientAAfficher].getDateDeNaissance());
			System.out.println(clients[indexClientAAfficher].getEmail());
		} else if(choix == 3){
			System.out.println(clients[indexClientAAfficher].getNom());
			System.out.println(clients[indexClientAAfficher].getPrenom());
			System.out.println(clients[indexClientAAfficher].getDateDeNaissance());
			System.out.println(clients[indexClientAAfficher].getEmail());
		}
	}
	
}
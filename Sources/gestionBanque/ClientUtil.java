
import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.*;


public class ClientUtil {
	Scanner sc = new Scanner(System.in);
	static Random run = new Random();
	static int indexClient = 1;

	public static Client[] redimClients(Client[] tab ){	
		Client[] tmp = new Client[tab.length + 1 ];      
		for(int i=0; i<tmp.length-1;i++){
				tmp[i] = tab[i];                                          
			System.out.println();
		}                                                                        
		return tmp;
	}																	
																		 																						
	public static Client[] creerClient1(Client[] clients){   				
		Client client1= new Client();
		clients[0]= client1;
		client1.setId();
		client1.setNom("DUPONT");
		client1.setPrenom("Nina");
		client1.setDateDeNaissance("15/03/1993");
		client1.setEmail("nina-dupont@gmail.com");
		
		return clients;
	}
	
	
		public static boolean existWithId(Client[]tab,  String id){
		boolean exist= false;
		for(int i=0; i<tab.length; i++){
				if(tab[i].getId().equals(id)){
					exist= true;
				}
			}
			return exist;
	}

	
	public static Client[] creerClient( Client[] clients, Scanner sc){
		clients= redimClients(clients);
		Client client = new Client();
		client.setId();
		clients[indexClient]= client;
		System.out.println(" Saisir le nom du client : ");
		String nomClient =sc.nextLine().toUpperCase();
		nomClient= ExceptionUtils.checkValue(nomClient, sc);
		client.setNom(nomClient);
		
		System.out.println(" Saisir le prenom du client : ");
		String prenom =sc.nextLine();
		prenom=ExceptionUtils.checkValue(prenom, sc);
		client.setPrenom(prenom);
		
		System.out.println(" Saisir la date de naissance du client : ");
		String annif =sc.nextLine();
		annif=ExceptionUtils.checkValue(annif, sc);
		client.setDateDeNaissance(annif);
		
		System.out.println(" Saisir l email du client : ");
		String email =sc.nextLine().trim();
		email=ExceptionUtils.checkValue(email, sc);
		client.setEmail(email);
		indexClient++;
		System.out.println("le client a bien ete cree");
		return clients;	
	}	

	public static void afficherClients(Client[] clients){	
			System.out.println(System.lineSeparator());
			System.out.printf("|%-15s|", "Index");
			System.out.printf("%-20s|", "Identifiant");
			System.out.printf("%-20s|", "Nom");
			System.out.printf("%-20s|", "Prenom");	
			System.out.printf("%-25s|", "Date_de_naissance");
			System.out.printf("%-30s|", "Email");			
			System.out.println();
			System.out.print("_____________________________________________________________________________________________________________________________________");
			System.out.println();		
			for (int i=0 ; i<clients.length; i++){		
				System.out.printf("|%-15s|", i);				
				System.out.printf("%-20s|", clients[i].getId());		
				System.out.printf("%-20s|", clients[i].getNom());
				System.out.printf("%-20s|", clients[i].getPrenom());	
				System.out.printf("%-25s|", clients[i].getDateDeNaissance());		
				System.out.printf("%-30s|", clients[i].getEmail());				
				System.out.println();
		}
		System.out.println(System.lineSeparator());
	}

	public static Client[] modifierClient(Client[] clients, String idClient , Scanner sc){
		int indexClientModifier= -1;
		String choixModif;
		for(int i=0; i<clients.length;i++){
			if(idClient.equals(clients[i].getId())){
				indexClientModifier=i;
			}
		}		
		do{
			System.out.println("Taper: "+ "\n" + 
							"1 =>  Modifier nom " + "\n" +
							"2 =>  Modifier prenom  " + "\n" +
							"3 =>  Date de naissance " + "\n" + 
							"4 =>  Modifier email");
			choixModif = sc.nextLine();
			choixModif= ExceptionUtils.checkValue(choixModif,sc);
			
			if(choixModif.equals("1")){
				System.out.println("Veuillez saisir le nouveau nom");
				String newNom= sc.nextLine();
				newNom= ExceptionUtils.checkValue(newNom,sc).toUpperCase();
				clients[indexClientModifier].setNom(newNom);
			}
			else if(choixModif.equals("2")){
				System.out.println("Veuillez saisir le nouveau prenom");
				String newPrenom= sc.nextLine();
				newPrenom= ExceptionUtils.checkValue(newPrenom,sc);
				clients[indexClientModifier].setNom(newPrenom);
			}
			else if(choixModif.equals("3")){
				System.out.println("Veuillez saisir la nouvelle date de naissance");
				String newDateNaiss= sc.nextLine();
				newDateNaiss= ExceptionUtils.checkValue(newDateNaiss,sc);
				clients[indexClientModifier].setDateDeNaissance(newDateNaiss);
			}
			else if(choixModif.equals("4")){
				System.out.println("Veuillez saisir la nouvelle adresse email");
				String newEmail= sc.nextLine();
				newEmail= ExceptionUtils.checkValue(newEmail,sc).trim();
				clients[indexClientModifier].setEmail(newEmail);
			}else{
				System.out.println("Veuillez saisir un choix valide ");
			}
		}while(( (!(choixModif.equals("1"))==true)  && (!(choixModif .equals("2"))==true) 
			&& (!(choixModif .equals("3"))==true)) && (!(choixModif .equals("4"))==true));
		System.out.println("le client a bien ete modifie");
	return clients;
	}
	
	public static String recuperIdClient(Client[] clients,Scanner sc){
		String idClient;
		Boolean saisieValide= false;
		 do {
			afficherClients(clients);
			System.out.println("Veuillez saisir l'id client svp :");
			idClient= sc.nextLine();
			idClient= ExceptionUtils.checkValue(idClient,sc).trim();
			for(int i=0; i<clients.length;i++){
					if((clients[i].getId()).equals(idClient)){
						saisieValide= true;
					}
			}
		 }while(saisieValide==false);
		
		return idClient;
	}
	
	public static Client[] supprimerClient(Client[] clients,String idClient){
		int indexClient1;
		for(int i=0; i<clients.length;i++){
			if(idClient.equals(clients[i].getId())){
				indexClient1=i;
			}
		}			
		Client[] newTab = new Client[clients.length-1];
		for(int i=0; i<newTab.length;i++){
			if(i <indexClient){
				newTab[i]= clients[i]; 
			}else{
				newTab[i]= clients[i+1];
			}
		}
		System.out.println(" le client dont id " +idClient+ " a bien ete supprimee");
		indexClient-- ;
		return newTab;
	}									
	

	public static void rechercherClient(Client[] clients,CompteBancaire[] comptesBancaires ,  Scanner sc){
		afficherClients(clients);
		String choixSeq ;
		String numeroDeCompte;
		do{
		System.out.println("Taper: "+ "\n" + 
						"1 =>  Rechercher client par nom " + "\n" +
						"2 =>  Rechercher client par numero de compte  " + "\n" +
						"3 =>  Rechercher client par identifiant ");
		choixSeq = sc.nextLine();
		choixSeq= ExceptionUtils.checkValue(choixSeq,sc);
		if(choixSeq.equals("1")){
			rechercheClientParNom(clients,comptesBancaires, sc);	
		}else if(choixSeq.equals("2")){
			rechercheClientParNumeroDeCompte(clients, comptesBancaires, sc);                                                
		}
		else if((choixSeq.equals("3"))){
			rechercheClientParIdentifiant(clients, sc);	
		}else {
			System.out.println("votre choix n'est pas valide ");
		}
		}while( (!(choixSeq.equals("1"))==true)  && (!(choixSeq .equals("2"))==true) && (!(choixSeq .equals("3"))==true));
	}
	public static void rechercheClientParNom(Client[] clients,CompteBancaire[] comptesBancaires,Scanner sc){
		String choixSeq;
		boolean existClientNom= false;
		System.out.println("Veuillez entrer le nom");
		String nom = sc.nextLine();
		nom= ExceptionUtils.checkValue(nom,sc).trim().toUpperCase();
		for(int i=0; i<clients.length; i++){
			if((clients[i].getNom()).equals(nom)){
				existClientNom= true;
				System.out.println("Index                 |"+i);
				System.out.println("Identifiant_client    |" +clients[i].getId());
				System.out.println("Nom                   |" +clients[i].getNom());
				System.out.println("Prenom                |" +clients[i].getPrenom());
				System.out.println("Date_Naissance        |" +clients[i].getDateDeNaissance());
				System.out.println("Email                 |" +clients[i].getEmail());
				break;
			}																			
		}
		if(existClientNom==false){                                                              
			System.out.println("Il n'existe aucun client avec cet Id !"); 
			System.out.println("Taper: "+ "\n" + 
						"oui    =>  Reessayer " + "\n" +
						"autre  =>  Quitter le programme ");
			choixSeq= sc.nextLine();
			choixSeq=ExceptionUtils.checkValue(choixSeq,sc).trim();
			
			if(choixSeq.equals("oui")){
				 rechercherClient(clients,comptesBancaires ,sc);
			}else{
				System.exit(0);
			}
		} 
		
	}
	public static void rechercheClientParIdentifiant(Client[] clients, Scanner sc){
		String choixSeq;
		boolean existClientId= false;
		System.out.println("Veuillez entrer l'identifiant");
		String identifiant = sc.nextLine();
		identifiant= ExceptionUtils.checkValue(identifiant,sc).trim();
		for(int i=0; i<clients.length; i++){
			if((clients[i].getId()).equals(identifiant)){
				existClientId= true;
			System.out.println("Index                 |"+i);
			System.out.println("Identifiant_client    |" +clients[i].getId());
			System.out.println("Nom                   |" +clients[i].getNom());
			System.out.println("Prenom                |" +clients[i].getPrenom());
			System.out.println("Date_Naissance        |" +clients[i].getDateDeNaissance());
			System.out.println("Email                 |" +clients[i].getEmail());
				break;
			}																										
		}
		if(existClientId==false){
			System.out.println("Il n'existe aucun client avec cet Id ! ");
			System.out.println("Taper: "+ "\n" + 
						"oui    =>  Reesseyer " + "\n" +
						"autre  =>  Quitter le programme ");																		
			choixSeq= sc.nextLine();
			choixSeq=ExceptionUtils.checkValue(choixSeq,sc).trim().toLowerCase();
			if(choixSeq.equals("oui")){
				 rechercheClientParIdentifiant(clients, sc);
			}else{
				System.exit(0);
			}
		}
	}  
	public static void rechercheClientParNumeroDeCompte(Client[]clients, CompteBancaire[] comptesBancaires, Scanner sc){
		String idClient= "";
		String numeroDeCompte;
		boolean compteExist;
		CompteBancaireUtil.afficherComptesBancaires(comptesBancaires);
		do{
			compteExist= false;
			System.out.println("Veuillez entrer le numero de compte");
			numeroDeCompte = sc.nextLine();
			numeroDeCompte=ExceptionUtils.checkValue(numeroDeCompte,sc).trim();
			for(int i=0; i<comptesBancaires.length; i++){
				if((comptesBancaires[i].getNumeroCompte()).equals(numeroDeCompte)){
					idClient=comptesBancaires[i].getIdClient();
					compteExist= true;
					break;
				}
			}
			if(compteExist==false){
				System.out.println("Il n'existe aucun compte avec cet numero");
			}		
		}while(compteExist==false);
		if(compteExist==true){
		rechercheClientParIdentifiant(clients,idClient);	
		}		
	}	
		public static void rechercheClientParIdentifiant(Client[] clients, String idClient){
		boolean existClient= false;
		for(int i=0; i<clients.length; i++){
			if((clients[i].getId()).equals(idClient)){
				existClient= true;
				System.out.println("Index                 |"+i);
				System.out.println("Identifiant_client    |" +clients[i].getId());
				System.out.println("Nom                   |" +clients[i].getNom());
				System.out.println("Prenom                |" +clients[i].getPrenom());
				System.out.println("Date_Naissance        |" +clients[i].getDateDeNaissance());
				System.out.println("Email                 |" +clients[i].getEmail());
				break;
			}																										
		}	
	} 	
}

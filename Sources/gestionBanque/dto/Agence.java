package gestionBanque.dto;
	
	public class Agence {
		private Integer code;
		private String nom;
		private String adresse;

		public Agence() {
		}
		public Agence(Integer code,String nom,String adresse) {
			this.code =code;
			this.nom = nom;
			this.adresse = adresse;
		}
	
		
		public Integer getCode() {
			return code;
		}

		public void setCode(Integer code) {
			this.code =code;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getAdresse() {
			return adresse;
		}

		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}
		
		public String toString() {
			return "Agence [code agence=" + code + ", nom agence=" + nom + ", adresse=" + adresse + "]";
		}
		
	}

package gestionBanque.dto;
import java.util.Random;

public class CompteBancaire {
	private String numeroCompte ;
	private Integer codeAgence;
	private String idClient ;
	private String type ;
	private Double solde;
	private Boolean decouvertEstAutorise; 
	
	public CompteBancaire () {
	}
	public CompteBancaire(Integer codeAgence, String idClient, String type, Double solde, 
			Boolean decouvertEstAutorise) {
		this.numeroCompte = genererNumeroCompte();
		this.codeAgence = codeAgence;
		this.idClient = idClient;
		this.type = type;
		this.solde = solde;
		this.decouvertEstAutorise = decouvertEstAutorise;
	}
	
	public String getNumeroCompte() {
		return this.numeroCompte;
	}
	public void setNumeroCompte() {
		this.numeroCompte = genererNumeroCompte() ;
	}

	public int getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(int codeAgence) {
		this.codeAgence = codeAgence;
	}
	public String getIdClient() {
		return idClient;
	}

	public void setIdClient(String idClient) {
		this.idClient = idClient;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Double getSolde() {
		return solde;
	}
	public void setSolde(Double solde) {
		this.solde = solde ;
	}
	public Boolean getDecouvertEstAutorise() {
		return decouvertEstAutorise;
	}
	public void setDecouvertEstAutorise(Boolean decouvertEstAutorise) {
		this.decouvertEstAutorise = decouvertEstAutorise;
	}

	public String genererNumeroCompte() {
		Random run = new Random();
			int  runChiffres = (run.nextInt(9999999+1-1000000)+1000000);
			String numCompte = "2022"+ runChiffres;
			return numCompte;
		 
		}
	
	public String toString() {
		return "Compte Bancaire [NumeroCompte=" + numeroCompte + " code agence=" + codeAgence + " id client =" + idClient 
		+ " typeCompte=" + type + "Solde=" + solde
				+ " decouvertEstAutorise=" + decouvertEstAutorise + "]";
	}

}

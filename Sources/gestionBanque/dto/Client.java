package gestionBanque.dto;
import java.util.Random;

public class Client {
	private String id;
	private String nom;
	private String prenom; 
	private String dateDeNaissance;
	private String email;
	private CompteBancaire[] comptesClient = new CompteBancaire[3];
	
	public Client() {
	}
	
	public Client(String nom, String prenom, String dateDeNaissance, String email){
		this.id=genererIdClient();
		this.nom = nom;
		this.prenom = prenom;
		this.dateDeNaissance = dateDeNaissance;
		this.email = email;
	}

	public String getId() {
		return id;
	}

	public void setId() {
		this.id = genererIdClient();
	}

	private String genererIdClient(){
		Random run = new Random();
		char maj1 =(char)((run.nextInt(90+1-65)) +65 );
		char maj2 = (char)((run.nextInt(90+1-65)) +65 );
		String runMaj =""+ maj1 + maj2;
		String runChiffres = "" + (run.nextInt(999999+1-100000)+100000);  
		String idClient = ""+ runMaj + runChiffres;
		return idClient;
													
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(String dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String toString() {
		return "Client [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", dateDeNaissance=" + dateDeNaissance
				+ ", email=" + email+" ]";
	}

}

package gestionBanque.views;

import gestionBanque.PrincipalBanque;
import gestionBanque.dto.Agence;
import gestionBanque.services.ExceptionUtils;

public class AgenceView {
	private static AgenceView instance;

	public static AgenceView getInstance() {
		if (instance == null)
			instance = new AgenceView();
		return instance;
	}

	private AgenceView() {
		//super();
	}

	public Agence recupererInfosAgence() {
		String nom = null;
		String adresse = null;
		
		System.out.println("Veuillez entrer le nom de l'agence");
		nom =PrincipalBanque.sc.nextLine();
		nom=ExceptionUtils.checkValue(nom, PrincipalBanque.sc);
		
		System.out.println("Veuillez entrer l'adresse de l'agence");
		adresse =PrincipalBanque.sc.nextLine();
		adresse=ExceptionUtils.checkValue(adresse, PrincipalBanque.sc);

		return new Agence(0,nom, adresse);

	}

	public static void afficherAgences(Agence[] agences){		
		System.out.println(System.lineSeparator());
		System.out.printf("|%-20s|", "Index");
		System.out.printf("%-20s|", "Code");
		System.out.printf("%-20s|", "Nom");
		System.out.printf("%-20s|", "Adresse");	
		System.out.println();
		System.out.print("____________________________________________________________________________________");
		System.out.println();		
		for (int i=0 ; i<agences.length; i++){		
			System.out.printf("|%-20s|", i);				
			System.out.printf("%-20s|", agences[i].getCode());		
			System.out.printf("%-20s|", agences[i].getNom());
			System.out.printf("%-20s|", agences[i].getAdresse());			
			System.out.println();
		
	}
	System.out.println(System.lineSeparator());
	}
	
	public static String recupererIndexAgence(Agence[] agences){
		Boolean notExist = false;
		afficherAgences(agences);
		String indexAgenceS;
		int indexAgence = 1000;
		do{
			notExist = false;
			System.out.println("Veuillez saisir un index");
			 indexAgenceS= PrincipalBanque.sc.nextLine();
			indexAgenceS= ExceptionUtils.checkValue(indexAgenceS,PrincipalBanque.sc);
			indexAgence=  Integer.parseInt(indexAgenceS);
			if(indexAgence == 0){
				System.out.println("Impossible de modifier ou supprimer l'agence par defaut");	
				notExist = true;
			}		
			if(indexAgence>agences.length-1){
				notExist = true;
				System.out.println("index invalide, veuillez saisir un index valide");
			}
		} while(notExist == true);
		return  indexAgenceS;
	}
	
	public String recupererNewInfoAgence(){
		String nouvelleInfo= null;
		do {
			System.out.println("Taper: "+ "\n" + 
							"1 =>  Modifier nom " + "\n" +
							"2 =>  Modifier adresse  " + "\n" );
			PrincipalBanque.choixSeq = PrincipalBanque.sc.nextLine();
			PrincipalBanque.choixSeq= ExceptionUtils.checkValue(PrincipalBanque.choixSeq,PrincipalBanque.sc);
			if(PrincipalBanque.choixSeq.equals("1")){
					System.out.println("Veuillez entrer le nouveau nom");
					nouvelleInfo = PrincipalBanque.sc.nextLine();
					nouvelleInfo= ExceptionUtils.checkValue(nouvelleInfo,PrincipalBanque.sc);
					
			}else if(PrincipalBanque.choixSeq.equals("2")){
					System.out.println("Veuillez entrer la nouvelle adresse");
					nouvelleInfo = PrincipalBanque.sc.nextLine();
					nouvelleInfo= ExceptionUtils.checkValue(nouvelleInfo,PrincipalBanque.sc);
			}else{
				System.out.println("Veuillez saisir un choix valide ");
			}
		}while( (!(PrincipalBanque.choixSeq.equals("1"))==true)  && (!(PrincipalBanque.choixSeq.equals("2"))==true)); 
		
		return nouvelleInfo;
	}

}

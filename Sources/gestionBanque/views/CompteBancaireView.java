package gestionBanque.views;

import gestionBanque.PrincipalBanque;
import gestionBanque.dto.*;
import gestionBanque.services.ExceptionUtils;
import gestionBanque.views.*;
import gestionBanque.dao.*;

public class CompteBancaireView {
	private static CompteBancaireView instance;
	private static AgenceView agenceView = AgenceView.getInstance();
	private static AgenceDao agenceDao = AgenceDao.getInstance();
	private static ClientDao clientDao = ClientDao.getInstance();
	private static ClientView clientView = ClientView.getInstance();

	public static CompteBancaireView getInstance() {
		if (instance == null)
			instance = new CompteBancaireView();
		return instance;
	}

	private CompteBancaireView() {
	}

	public static CompteBancaire recupererInfosCompteBancaire(CompteBancaire[]comptesBancaires,Agence[] agences ,Client[] clients) {
		Integer code= null;
		String id= null;
		String type= null;
		Double solde= null;
		Boolean decouvert= null;
		
		boolean existClient= false;
		boolean possede = false;
		boolean existAgence=false;
		String decouvertS="";
		String  message = "";
		do{
			System.out.println("Veuillez entrer un code d'agence parmis les codes qui s'affichent :");
			agenceView.afficherAgences(agences);
			String codeS= PrincipalBanque.sc.nextLine();
			codeS= ExceptionUtils.checkValue(codeS, PrincipalBanque.sc);
			code= Integer.parseInt(codeS);
			existAgence= agenceDao.existWithCode(agences,code);     
		}while(existAgence==false);
	
		clientView.afficherClient(clients);
		do{
			System.out.println("Veuillez entrer un id client parmis les clients qui s'affichent :");
			System.out.println(" faite un copier coller de id svp !!!");     
			id=PrincipalBanque.sc.nextLine();
			id= ExceptionUtils.checkValue(id, PrincipalBanque.sc).trim();			
			existClient= clientDao.existWithId(clients, id);    			        
			if(existClient==false){
			System.out.println("Il n'existe aucun client avec cet Id !"); 
			}
		}while(existClient==false);
		
		
		do {
			do{
				possede= false;
				message= "";
				System.out.println("Taper: "+ "\n" + 
									"CC =>  Compte Courant " + "\n" +
									"PE =>  Plan d E pargne " + "\n" +
									"LA =>  Livret A");
									
				type = PrincipalBanque.sc.nextLine().trim();
				type = ExceptionUtils.checkValue(type, PrincipalBanque.sc);
				for (int i=0 ; i<comptesBancaires.length; i++){		
					if((comptesBancaires[i].getIdClient().equals(id)==true) && (comptesBancaires[i].getType().equals(type)==true)){
						message = "le client dont l id= " + id +" possede deja un compte de type " + type;
						possede = true ;	
					}
				System.out.println(message);	
				}
			}while(possede == true );
			if ((!(type.equals("CC"))==true)  && (!(type.equals("PE"))==true) && (!(type.equals("LA"))==true)){
				System.out.println("Votre saisie n'est pas valide!");
			}
		}while ( (!(type.equals("CC"))==true)  && (!(type.equals("PE"))==true) && (!(type.equals("LA"))==true))  ;
	
		System.out.println("Veuillez saisir le solde ");
		String soldeS= PrincipalBanque.sc.nextLine().trim();
		soldeS= ExceptionUtils.checkValue(soldeS, PrincipalBanque.sc);
		solde = Double.parseDouble(soldeS);
		if(type.equals("CC")){
			solde -= 25 ;
		}
		else if(type.equals("LA")){
			solde-=25+(0.1*solde);	
		}
		else{
			solde-=25+(0.25*solde);
		}

		do{
		System.out.println("Decouvert est Autorise ?: oui  /  non  ");
		decouvertS=  PrincipalBanque.sc.nextLine();
		decouvertS= ExceptionUtils.checkValue(decouvertS, PrincipalBanque.sc);
		decouvertS=decouvertS.trim().toLowerCase();
		if((!(decouvertS.equals("oui"))==true)  && (!(decouvertS.equals("non"))==true)){
				System.out.println("Veuillez saisir un choix valide ");
			}
		}while( (!(decouvertS.equals("oui"))==true)  && (!(decouvertS.equals("non"))==true));
		if (decouvertS.equals("oui")==true){
			decouvert=true;	
		}else{
			decouvert=false;
		}
		return new CompteBancaire(code, id,type ,solde ,decouvert);

	}

	public void afficherComptesBancaires(CompteBancaire[] comptesBancaires){
		System.out.println(System.lineSeparator());
		System.out.printf("|%-10s|", "Index");
		System.out.printf("%-20s|", "Numero_compte");
		System.out.printf("%-20s|", "Code_agence");
		System.out.printf("%-20s|", "Id_client ");
		System.out.printf("%-10s|", "Type");
		System.out.printf("%-20s|", "Solde");		
		System.out.printf("%-20s|", "Decouvert autorise");
		System.out.println();
		System.out.print("________________________________________________________________________________________________________________________________");
		System.out.println();	
		for (int i=0 ; i<comptesBancaires.length; i++){		
			System.out.printf("|%-10s|", i);				
			System.out.printf("%-20s|", comptesBancaires[i].getNumeroCompte());		
			System.out.printf("%-20s|", comptesBancaires[i].getCodeAgence());
			System.out.printf("%-20s|", comptesBancaires[i].getIdClient());
			System.out.printf("%-10s|", comptesBancaires[i].getType());
			System.out.printf("%-20s|", comptesBancaires[i].getSolde());
			System.out.printf("%-20s|", comptesBancaires[i].getDecouvertEstAutorise());
			System.out.println();
	}
	System.out.println(System.lineSeparator());
	}
	
	public String recupererIndexCompteBancaire(CompteBancaire[] comptesBancaires){
		Boolean notExist = false;
		afficherComptesBancaires(comptesBancaires);
		String indexCompteBancaireS;
		int indexCompteBancaire = 100;
		do{
			notExist = false;
			System.out.println("Veuillez saisir un index");
			 indexCompteBancaireS= PrincipalBanque.sc.nextLine();
			indexCompteBancaireS= ExceptionUtils.checkValue(indexCompteBancaireS,PrincipalBanque.sc);
			indexCompteBancaire=  Integer.parseInt(indexCompteBancaireS);	
			if(indexCompteBancaire>comptesBancaires.length-1){
				notExist = true;
				System.out.println("index invalide, veuillez saisir un index valide");
			}
		} while(notExist == true);
		return  indexCompteBancaireS;
	}
	
	public String recupererNumeroCompteBancaire(CompteBancaire[]comptesBancaires){
		for(int i=0; i<comptesBancaires.length; i++){
			System.out.println(comptesBancaires[i].getNumeroCompte());
		}
		boolean compteExist= false;
		String NumCompteSaisie;
		do {
			System.out.println(" Saisir le numero de compte a rechercher : ");
			NumCompteSaisie = PrincipalBanque.sc.nextLine();
			NumCompteSaisie= ExceptionUtils.checkValue(NumCompteSaisie,PrincipalBanque.sc).trim();
			for(int i=0; i<comptesBancaires.length; i++){
				if(comptesBancaires[i].getNumeroCompte().equals(NumCompteSaisie)==true){
					compteExist= true;	
				}
			}
			if(compteExist==false){
				System.out.println("Il n'existe aucun compte avec cet numero"); 
			}
		}while(compteExist==false);
		return NumCompteSaisie;
		
	}
	public void afficherCompte( CompteBancaire compte){
		System.out.println(System.lineSeparator());
		System.out.printf("%-20s|", "Numero_compte");
		System.out.printf("%-20s|", "Code_agence");
		System.out.printf("%-20s|", "Id_client ");
		System.out.printf("%-10s|", "Type");
		System.out.printf("%-20s|", "Solde");		
		System.out.printf("%-20s|", "Decouvert autorise");
		System.out.println();
		System.out.print("______________________________________________________________________________________________________________________");
		System.out.println();					
		System.out.printf("%-20s|", compte.getNumeroCompte());		
		System.out.printf("%-20s|", compte.getCodeAgence());
		System.out.printf("%-20s|", compte.getIdClient());
		System.out.printf("%-10s|", compte.getType());
		System.out.printf("%-20s|", compte.getSolde());
		System.out.printf("%-20s|", compte.getDecouvertEstAutorise());
		System.out.println();
	}
	
	public String recupererNewInfoCompteBancaire( CompteBancaire[]comptesBancaires, int pos){
		String nouvelleInfo= null;
		do {
			System.out.println("Taper: "+ "\n" + 
							"1 =>  Modifier Solde" + "\n" +
							"2 =>  Modifier Decouvert autorise  " + "\n" );
			PrincipalBanque.choixSeq = PrincipalBanque.sc.nextLine();
			PrincipalBanque.choixSeq= ExceptionUtils.checkValue(PrincipalBanque.choixSeq,PrincipalBanque.sc);
			
			if(PrincipalBanque.choixSeq.equals("1")){
					System.out.println("Veuillez entrer le nouveau solde");
					nouvelleInfo = PrincipalBanque.sc.nextLine();
					nouvelleInfo= ExceptionUtils.checkValue(nouvelleInfo,PrincipalBanque.sc);
					
			}else if(PrincipalBanque.choixSeq.equals("2")){
				if(comptesBancaires[pos].getDecouvertEstAutorise()==true){
					System.out.println("Voulez-vous cesser d'autoriser le decouvert ?");
					nouvelleInfo = PrincipalBanque.sc.nextLine();
					nouvelleInfo=ExceptionUtils.checkValue(nouvelleInfo,PrincipalBanque.sc).trim().toLowerCase();
				
				}else{
					System.out.println("Voulez-vous autoriser le decouvert ?");
					nouvelleInfo = PrincipalBanque.sc.nextLine();
					nouvelleInfo=ExceptionUtils.checkValue(nouvelleInfo,PrincipalBanque.sc).trim().toLowerCase();
				}			
			}else{
				System.out.println("Veuillez saisir un choix valide ");
			}
		}while( (!(PrincipalBanque.choixSeq.equals("1"))==true)  && (!(PrincipalBanque.choixSeq.equals("2"))==true)); 
		
		return nouvelleInfo;
	}
}

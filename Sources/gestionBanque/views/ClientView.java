package gestionBanque.views;

import gestionBanque.PrincipalBanque;
import gestionBanque.dto.Client;
import gestionBanque.dto.CompteBancaire;
import gestionBanque.services.ExceptionUtils;
import java.util.*;

public class ClientView{
	private static ClientView instance;

	public static ClientView getInstance() {
		if (instance == null)
			instance = new ClientView();
		return instance;
	}
	public String recupererIdClient(Client[] clients){
		boolean existClient= false; 
		String id ;
		afficherClient(clients);
		do {
		System.out.println("Veuillez saisir l'Id du client");
		id = PrincipalBanque.sc.nextLine();
		id=ExceptionUtils.checkValue(id, PrincipalBanque.sc);
		for(int i=0; i<clients.length;i++){
			if(clients[i].getId().equals(id)){
				existClient=true;
			}
		}
		if(existClient==false)
			System.out.println("Il n'existe aucun client avec cet Id !"); 
		}while(existClient==false);
		return id; 
	}
	
	public int recupererIndexClient(Client[] clients){
		String indexClient;
		int index;
		boolean nombreCorrect = true;
		do{
			if(nombreCorrect == false){
				System.out.println("\n [NOMBRE INCORRECT] Veuillez saisir un index de client contenu dans l'affichage ci-dessus \n \n \n");
			}
			System.out.println("Veuillez inserer l'index du client");
			indexClient = PrincipalBanque.sc.nextLine();
			indexClient=ExceptionUtils.checkValue(indexClient, PrincipalBanque.sc);
			
			index = Integer.parseInt(indexClient);
			nombreCorrect = false;
		}while(((index<0)==true) && ((index>clients.length-1)==true));
		
		return index;
	}
	
	public int recupererChoix(){
		
		String choix;
		int choixClient;
		boolean badChoice = false;
		do{
			System.out.println("Par quel element souhaitez-vous chercher ? \n \n" +
										"Taper: \n \n" + 
										"1 =>  Rechercher par nom " + "\n" +
										"2 =>  Rechercher par numero de compte  " + "\n" +
										"3 =>  Rechercher par identifiant de client  " + "\n");
			choix = PrincipalBanque.sc.nextLine();
			choix=ExceptionUtils.checkValue(choix, PrincipalBanque.sc);
			choixClient = Integer.parseInt(choix);
			
			if((choixClient!=1) && (choixClient!=2) && (choixClient!=3)){
				badChoice = true;
			}
			if(badChoice == true){
				System.out.println("\n [CHOIX INCORRECT] Veuillez inserer un chiffre compris entre 1 et 3 \n \n \n");
			}
		}while(badChoice == true);
		
		return choixClient;
		
	}
	
	public int recupererElementClient(int choix, Client[] clients, CompteBancaire[] comptesBancaires){
		String elementClient = "";
		int indexClientAAfficher = 0;
		String idClient = "";
		boolean exist = false;
		if(choix == 1){
				do{
					System.out.println("Veuillez inserer un nom");
					elementClient = PrincipalBanque.sc.nextLine().toUpperCase();
					for(int i=0; i<clients.length;i++){
						if((clients[i].getNom()).equals(elementClient)){
							i = indexClientAAfficher;
							exist = true;
							break;
						}
					}
					if(exist == false){
						System.out.println("\n [CHOIX INCORRECT] Veuillez inserer un nouveau nom \n \n \n");
					}
				}while(exist == false);
				exist = false;
		}  else if(choix == 2){
				do{
					exist = false;
					System.out.println("Veuillez entrer un numero de compte");
					elementClient = PrincipalBanque.sc.nextLine();
					for(int i=0; i<comptesBancaires.length;i++){
						if((comptesBancaires[i].getNumeroCompte()).equals(elementClient)){
							idClient = comptesBancaires[i].getIdClient();
							exist = true;
							break;
						}
					}
					for(int i=0; i<clients.length;i++){
						if((clients[i].getId()).equals(idClient)){
							i = indexClientAAfficher;
							break;
						}
					}
				}while(exist == false);
				exist = false;
		} else if(choix == 3){
			do{
				System.out.println("Veuillez inserer un identifiant");
				elementClient = PrincipalBanque.sc.nextLine();
				for(int i=0; i<clients.length;i++){
					if((clients[i].getId()).equals(elementClient)){
						i = indexClientAAfficher;
						exist = true;
						break;
					}
				}
				if(exist == false){
					System.out.println("\n [CHOIX INCORRECT] Veuillez inserer un nouveau nom \n \n \n");
				}
			}while(exist == false);
		}
		return indexClientAAfficher;
	}
	
	public int recupererElementAModifier(){
		int elementAModifier;
		boolean chiffreCorrect = false;
		do{
			if(chiffreCorrect == true){
				System.out.println("\n [CHOIX INCORRECT] Veuillez inserer un chiffre compris entre 1 et 4 \n \n \n");
			}
			System.out.println("Quel element souhaitez-vous modifier ? \n \n" +
									"Taper: \n \n" + 
									"1 =>  Modifier nom " + "\n" +
									"2 =>  Modifier prenom  " + "\n" +
									"3 =>  Modifier date de naissance  " + "\n" +
									"4 =>  Modifier email  " + "\n");
			elementAModifier = Integer.parseInt(PrincipalBanque.sc.nextLine());
			if((elementAModifier!=1) && (elementAModifier!=2) && (elementAModifier!=3) && (elementAModifier!=4)){
				chiffreCorrect = true;
			}
		}while(chiffreCorrect == true);
		return elementAModifier;
	}
	
	public String recupererNouvelleElement(int elementModif){
		String nouveauElement = "";
		if(elementModif == 1){
			System.out.println("Veuillez inserer un nouveau nom");
			nouveauElement = PrincipalBanque.sc.nextLine();
			nouveauElement=ExceptionUtils.checkValue(nouveauElement, PrincipalBanque.sc);
		} else if(elementModif == 2){
			System.out.println("Veuillez inserer un nouveau prenom");
			nouveauElement = PrincipalBanque.sc.nextLine();
			nouveauElement=ExceptionUtils.checkValue(nouveauElement, PrincipalBanque.sc);
		} else if(elementModif == 3){
			System.out.println("Veuillez une nouvelle date de naissance");
			nouveauElement = PrincipalBanque.sc.nextLine();
			nouveauElement=ExceptionUtils.checkValue(nouveauElement, PrincipalBanque.sc);
		} else if(elementModif == 4){
			System.out.println("Veuillez inserer un nouveau email");
			nouveauElement = PrincipalBanque.sc.nextLine();
			nouveauElement=ExceptionUtils.checkValue(nouveauElement, PrincipalBanque.sc);
		}
		
		
		return nouveauElement;
	}
	
	public Client recupererInfosClient() {
	String nom = null;
	String prenom = null;
	String email = null;
	String dateDeNaissance = null;
	System.out.println("Reccuperation ifos client");
	System.out.println("Veuillez entrer le nom");
	nom =PrincipalBanque.sc.nextLine();
	nom=ExceptionUtils.checkValue(nom, PrincipalBanque.sc).toUpperCase();
	
	System.out.println("Veuillez entrer prenom");
	prenom = PrincipalBanque.sc.nextLine();
	prenom=ExceptionUtils.checkValue(prenom, PrincipalBanque.sc);
	
	System.out.println("Veuillez entrer email");
	email =PrincipalBanque.sc.nextLine();
	email=ExceptionUtils.checkValue(email, PrincipalBanque.sc).trim();
	
	System.out.println("Veuillez entrer date de naissance");
	dateDeNaissance =PrincipalBanque.sc.nextLine();
	dateDeNaissance=ExceptionUtils.checkValue(dateDeNaissance, PrincipalBanque.sc);

	return new Client(nom,prenom, email, dateDeNaissance);

	}
	
	public static void afficherClient(Client[] clients){		
	System.out.println(System.lineSeparator());
	System.out.printf("|%-20s|", "Index");
	System.out.printf("%-20s|", "Identifiant");
	System.out.printf("%-20s|", "Nom");
	System.out.printf("%-20s|", "Prenom"); 
	System.out.printf("%-20s|", "Date de naissance");	
	System.out.printf("%-20s|", "Email");	
	System.out.println();
	System.out.print("____________________________________________________________________________________________________________________________");
	System.out.println();		
	for (int i=0 ; i<clients.length; i++){		
		System.out.printf("|%-20s|", i);	
		System.out.printf("%-20s|", clients[i].getId());
		System.out.printf("%-20s|", clients[i].getNom());
		System.out.printf("%-20s|", clients[i].getPrenom());
		System.out.printf("%-20s|", clients[i].getDateDeNaissance());		
		System.out.printf("%-20s|", clients[i].getEmail());			
		System.out.println();
	
	}
	System.out.println(System.lineSeparator());
	}
	
}
package gestionBanque.controllers;
import gestionBanque.dao.*;
import gestionBanque.dto.Client;
import gestionBanque.dto.CompteBancaire;
import gestionBanque.views.ClientView;
import gestionBanque.PrincipalBanque;

public class ClientController{
	
	public ClientDao clientDao = ClientDao.getInstance();
	public  CompteBancaireDao compteBancaireDao = CompteBancaireDao.getInstance();
	public ClientView clientView = ClientView.getInstance();
	private static ClientController instance;
 
	public static ClientController getInstance(){
		if (instance == null)
			instance = new ClientController();
		return instance;
	}
	 
	public Client[] menuCreerClient(Client[]clients){
		Client client = clientView.recupererInfosClient();
		if (client != null) {
			clients = clientDao.save(client, clients); 
		}
		clientView.afficherClient(clients);
		return clients; 
	} 
	
	public Client[] menuSupprimerClient(Client[] clients, CompteBancaire[] comptesBancaires){
		clientView.afficherClient(clients);
		Integer indexClient = clientView.recupererIndexClient(clients);  
		if (indexClient!= null) {
			PrincipalBanque.idClientComptes = clients[indexClient].getId();
			clients = clientDao.supprimerClient(indexClient, clients);
		}
		clientView.afficherClient(clients);
		return clients;
	}
	
	public Client[] menuModifierClient(Client[] clients){
		clientView.afficherClient(clients);
		String nouvelleElement = "";
		int indexModif;
		int indexClientAModifier; 
		indexClientAModifier = clientView.recupererIndexClient(clients);
		indexModif = clientView.recupererElementAModifier();
		nouvelleElement = clientView.recupererNouvelleElement(indexModif);
		if(nouvelleElement != null){
			clients = clientDao.modifierClient(clients, nouvelleElement, indexModif, indexClientAModifier);
		}
		clientView.afficherClient(clients);
		return clients;
	}
	
	public void menuRechercherClient(Client[] clients, CompteBancaire[] comptesBancaires){
		int choix;
		int indexClientAAfficher;
		choix = clientView.recupererChoix();
		indexClientAAfficher = clientView.recupererElementClient(choix, clients, comptesBancaires);
		clientDao.rechercherElementClient(clients, comptesBancaires, choix, indexClientAAfficher); 
	}
	
	
	public void menuAffichageListe(Client[] clients){
		clientView.afficherClient(clients);
	}
}
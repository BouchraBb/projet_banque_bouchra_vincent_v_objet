package gestionBanque.controllers;
import gestionBanque.dao.AgenceDao;
import gestionBanque.dto.Agence;
import gestionBanque.dto.CompteBancaire;
import gestionBanque.views.AgenceView;
import gestionBanque.PrincipalBanque;
 
public class AgenceController {
	public AgenceDao agenceDao = AgenceDao.getInstance();
	public AgenceView agenceView = AgenceView.getInstance();
	private static AgenceController instance;

	public static AgenceController getInstance() {
		if (instance == null)
			instance = new AgenceController();
		return instance; 
	}

	private AgenceController(){
	}

	public Agence[] menuCreerAgence(Agence[]agences) {
		Agence agence = agenceView.recupererInfosAgence();
		if (agence != null) {
			agences = agenceDao.save(agence, agences);
		}
		agenceView.afficherAgences(agences);
		return agences;
	}

	public void menuAffichageListe(Agence[]agences) {
		agenceView.afficherAgences(agences);
	}
	
	public Agence[] menuModifierAgence(Agence[]agences){
		String indexAgenceAModifier = agenceView.recupererIndexAgence(agences);
		if (indexAgenceAModifier!=null){
			String newInfo = agenceView.recupererNewInfoAgence();
			if (newInfo!=null){
				agences = agenceDao.modifierAgence(agences, Integer.parseInt(indexAgenceAModifier), newInfo );
				System.out.println(" l'agence dont index = " +indexAgenceAModifier+ " a bien ete modifier");
			}
		}
		return agences;
	}
	public Agence[] menuSupprimerAgence(Agence[]agences, CompteBancaire[] comptesBancaires){
		String indexAgenceASupp = agenceView.recupererIndexAgence(agences);
		if(indexAgenceASupp!=null){
			agences = agenceDao.supprimerAgence(agences, Integer.parseInt(indexAgenceASupp));
			System.out.println(" l'agence dont index = " +indexAgenceASupp+ " a bien ete supprimee");
			comptesBancaires = agenceDao.migration(agences, comptesBancaires);
		}
		return agences;
	}
}

package gestionBanque.controllers;
import gestionBanque.dao.CompteBancaireDao;
import gestionBanque.dto.*;
import gestionBanque.views.*;
import gestionBanque.PrincipalBanque;

public class CompteBancaireController {
	public CompteBancaireDao compteBancaireDao = CompteBancaireDao.getInstance();
	public CompteBancaireView compteBancaireView = CompteBancaireView.getInstance();
	public ClientView clientView = ClientView.getInstance();
	private static CompteBancaireController instance;

	public static CompteBancaireController getInstance() {
		if (instance == null)
			instance = new CompteBancaireController();
		return instance;
	}

	private CompteBancaireController(){
	}

	public CompteBancaire[] menuCreerCompteBancaire(CompteBancaire[]comptesBancaires,Agence[] agences ,Client[] clients) {
		CompteBancaire compteBancaire = CompteBancaireView.recupererInfosCompteBancaire(comptesBancaires,agences,clients);
		if (compteBancaire != null) {
			comptesBancaires = compteBancaireDao.save(compteBancaire, comptesBancaires);
		}
		compteBancaireView.afficherComptesBancaires(comptesBancaires);
		
		return comptesBancaires;
	}

	public void menuAffichageListe(CompteBancaire[] comptesBancaires) {
		compteBancaireView.afficherComptesBancaires(comptesBancaires);
	}
	
	public CompteBancaire[] menuModifierCompteBancaire(CompteBancaire[]comptesBancaires){
		String  indexCompteBancaireAModifier = compteBancaireView.recupererIndexCompteBancaire(comptesBancaires);
		if (indexCompteBancaireAModifier!=null){
			String newInfo = compteBancaireView.recupererNewInfoCompteBancaire(comptesBancaires, Integer.parseInt(indexCompteBancaireAModifier));
			if (newInfo!=null){ 
				comptesBancaires = compteBancaireDao.modifierCompteBancaire
				(comptesBancaires, Integer.parseInt(indexCompteBancaireAModifier), newInfo );
				System.out.println(" le compte dont index = " +indexCompteBancaireAModifier+ " a bien ete modifier");
			}
		}
		return comptesBancaires;
	}
	
	
	public CompteBancaire[] menuSupprimerCompteBancaire(CompteBancaire[]comptesBancaires){
		String indexCompteBancaireASupp = compteBancaireView.recupererIndexCompteBancaire(comptesBancaires);
		if(indexCompteBancaireASupp!=null)
			comptesBancaires = compteBancaireDao.supprimerCompteBancaire(comptesBancaires, Integer.parseInt(indexCompteBancaireASupp));
		return comptesBancaires;
	}
	
	public void menuRechercherCompteParNumero(CompteBancaire[]comptesBancaires){
		CompteBancaire compte =null;
		String numeroCompte = compteBancaireView.recupererNumeroCompteBancaire(comptesBancaires);
		if(numeroCompte!= null){
			compte = compteBancaireDao.chercherCompteParNumero(comptesBancaires, numeroCompte);
		}
		if(compte!= null)
			compteBancaireView.afficherCompte(compte);
	}
	
	public void menuAfficherComptesClient(Client[]clients, CompteBancaire[] comptesBancaires){
		String id= null ; 
		CompteBancaire[] comptesClient=null  ;
		id = clientView.recupererIdClient(clients);
		if (id!=null){
		comptesClient = compteBancaireDao.chercherComptesClient(comptesBancaires, id);
		}
		if(comptesClient!=null)
		compteBancaireView.afficherComptesBancaires(comptesClient);
	}
}

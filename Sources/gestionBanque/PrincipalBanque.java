package gestionBanque;
import java.util.*;
import gestionBanque.controllers.*;
import gestionBanque.dto.*;
import gestionBanque.dao.*;
import gestionBanque.views.*;
import gestionBanque.services.Helpers;

public class PrincipalBanque {
	public static Scanner sc = new Scanner(System.in);
	public static Agence[] agences= new Agence[1];
	public static Client clients[]= new Client[1];
	public static CompteBancaire comptesBancaires[]= new CompteBancaire[1];
	public static String choixSeq= "";
	public static String idClientComptes;
	
	public static void main(String []args){
		String choix;
		String idClient;
		
		AgenceController agenceController = AgenceController.getInstance();
		AgenceDao agenceDao = AgenceDao.getInstance();
		
		ClientController clientController = ClientController.getInstance();
		ClientDao clientDao = ClientDao.getInstance();
		ClientView clientView = ClientView.getInstance();
		
		CompteBancaireController compteBancaireController = CompteBancaireController.getInstance();
		CompteBancaireDao compteBancaireDao = CompteBancaireDao.getInstance();
		
		agences= agenceDao.creerAgenceParDefaut(agences);  
		clients= clientDao.creerClient1(clients);
		comptesBancaires=compteBancaireDao.creerCompte1(comptesBancaires);
		do{
			System.out.println("-------------------------------    MENU BANQUE CDA JAVA   ------------------------------------------------");
			System.out.println("1- Creer une agence  ");
			System.out.println("2- Creer un client ");
			System.out.println("3- Creer un compte bancaire");
			System.out.println("4- Modifier une agence");
			System.out.println("5- Modifier un client");
			System.out.println("6- Modifier un compte bancaire");
			System.out.println("7- Supprimer une agence");
			System.out.println("8- Supprimer un client");
			System.out.println("9- Supprimer un compte bancaire");
			System.out.println("10- Afficher la liste de toutes les agences");
			System.out.println("11- Afficher la liste de toutes les comptes");
			System.out.println("12- Afficher la liste de tous les clients");
			System.out.println("13- Recherche de compte(numero de compte)");
			System.out.println("14- Recherche de client(Nom, Numero de compte, identifiant de client)");
			System.out.println("15- Afficher la liste des comptes d'un client(identifiant client)");
			System.out.println("16- Imprimer les infos client (identifiant client)");
			System.out.println("17- Quitter ");
			System.out.println("----------------------------------------------------------------------------------------------------------");
			System.out.println("Votre choix : ");	
			choix = sc.nextLine();
			if(choix.equals("1")){ 
				agences = agenceController.menuCreerAgence(agences);
			}
			else if(choix.equals("2")){
				clients= clientController.menuCreerClient(clients);
			}
			else if(choix.equals("3")){
				comptesBancaires= compteBancaireController.menuCreerCompteBancaire(comptesBancaires, agences, clients);
			}
			else if(choix.equals("4")){
				agences= agenceController.menuModifierAgence(agences);
			}
			else if(choix.equals("5")){
				clients=clientController.menuModifierClient(clients);
			}
			else if(choix.equals("6")){
				comptesBancaires= compteBancaireController.menuModifierCompteBancaire(comptesBancaires);
			}
			else if(choix.equals("7")){
				agences= agenceController.menuSupprimerAgence(agences, comptesBancaires);	
			}
			else if(choix.equals("8")){ 
				clients=clientController.menuSupprimerClient(clients,comptesBancaires);
				comptesBancaires=compteBancaireDao.supprimerComptesClient(comptesBancaires, idClientComptes);
			}
			else if(choix.equals("9")){
				comptesBancaires= compteBancaireController.menuSupprimerCompteBancaire(comptesBancaires);
			}
			else if(choix.equals("10")){
				agenceController.menuAffichageListe(agences);	
			}
			else if(choix.equals("11")){
				compteBancaireController.menuAffichageListe(comptesBancaires);
			}
			else if(choix.equals("12")){
				clientController.menuAffichageListe(clients);
			}
			else if(choix.equals("13")){
				compteBancaireController.menuRechercherCompteParNumero(comptesBancaires);
			}
			else if(choix.equals("14")){
				clientController.menuRechercherClient(clients, comptesBancaires);
			}
			else if(choix.equals("15")){
				compteBancaireController.menuAfficherComptesClient(clients, comptesBancaires);
			}
			else if(choix.equals("16")){
				Helpers.creationFichierClient(comptesBancaires, clients);
			}
			else if(choix.equals("17")){
				System.out.println("Au revoir");
			}
			else {
				System.out.println("Votre saisie n est pas valide veuillez saisir a nouveau ");
			}
			Helpers.genererFichierAgence(agences);
			Helpers.genererFichierClient(clients);
			Helpers.genererFichierCompteBancaire(comptesBancaires);
		}while(!(choix.equals("17")));
	}
}
package gestionBanque.services;
import java.util.*;
import java.io.*;
import gestionBanque.dto.*;
import gestionBanque.views.ClientView;
import gestionBanque.PrincipalBanque;

	public class Helpers{
		public static ClientView clientView = ClientView.getInstance();
		
		
		private static final String DELIMITER = ",";
		private static final String SEPARATOR = "\n";
		
		private static final String HEADERAGENCE = "Code,Nom,Adresse";
		private static final String HEADERCLIENT = "ID,Nom,Prenom, Date_Naissance, Email";
		private static final String HEADERCOMPTE = "Numero ,Code_agence ,Id_client, Type, Solde, Decouvert_Autorise";
		
		
		public static void genererFichierAgence(Agence[] agences){
			FileWriter fileAgences = null;
			try
			{
			fileAgences = new FileWriter("Agences.csv");
			fileAgences.append(HEADERAGENCE);
			fileAgences.append(SEPARATOR);
			for(int i=0; i<agences.length; i++){
				  fileAgences.append(""+agences[i].getCode());
				  fileAgences.append(DELIMITER);
				  fileAgences.append(agences[i].getNom());
				  fileAgences.append(DELIMITER);
				  fileAgences.append(agences[i].getAdresse());
				  fileAgences.append(SEPARATOR);
			}
			fileAgences.close();
			}
			catch(Exception e)
			{
			e.printStackTrace();
			}

		}
		public static void genererFichierClient(Client[] clients){
			FileWriter fileClients = null;
			try
			{
			fileClients = new FileWriter("Clients.csv");
			fileClients.append(HEADERCLIENT);
			fileClients.append(SEPARATOR);
			for(int i=0; i<clients.length; i++){
				  fileClients.append(clients[i].getId());
				  fileClients.append(DELIMITER);
				  fileClients.append(clients[i].getNom());
				  fileClients.append(DELIMITER);
				  fileClients.append(clients[i].getPrenom());
				  fileClients.append(DELIMITER);
				  fileClients.append(clients[i].getDateDeNaissance());
				  fileClients.append(DELIMITER);
				  fileClients.append(clients[i].getEmail());
				  fileClients.append(DELIMITER);
				  fileClients.append(SEPARATOR);
			}
			fileClients.close();
			}
			catch(Exception e)
			{
			e.printStackTrace();
			}
		}
		public static void genererFichierCompteBancaire(CompteBancaire[] comptesBancaires){
			FileWriter fileComptesBancaires = null;
							
			try
			{
			fileComptesBancaires = new FileWriter("ComptesBancaires.csv");
			fileComptesBancaires.append(HEADERCOMPTE);
			fileComptesBancaires.append(SEPARATOR);
			for(int i=0; i<comptesBancaires.length; i++){
				  fileComptesBancaires.append(comptesBancaires[i].getNumeroCompte());
				  fileComptesBancaires.append(DELIMITER);
				  fileComptesBancaires.append(""+comptesBancaires[i].getCodeAgence());
				  fileComptesBancaires.append(DELIMITER);
				  fileComptesBancaires.append(comptesBancaires[i].getIdClient());
				   fileComptesBancaires.append(DELIMITER);
				  fileComptesBancaires.append(comptesBancaires[i].getType());
				   fileComptesBancaires.append(DELIMITER);
				  fileComptesBancaires.append(""+comptesBancaires[i].getSolde());
				   fileComptesBancaires.append(DELIMITER);
				  fileComptesBancaires.append(""+comptesBancaires[i].getDecouvertEstAutorise());
				  fileComptesBancaires.append(SEPARATOR);
			}
			fileComptesBancaires.close();
			}
			catch(Exception e)
			{
			e.printStackTrace();
			}	
		}
		public static void creationFichierClient(CompteBancaire[] comptesBancaires, Client[] clients){
			int indexClientWriter = 0;
			clientView.afficherClient(clients);
			System.out.println("entrez l'Id client : ");
			String identifiant = PrincipalBanque.sc.nextLine();
			for(int i=0; i<clients.length; i++){
				if(identifiant.equals(clients[i].getId())){
					indexClientWriter = i;
					break;
				}
			}		
			
			File file = new File("../Sources/doc.txt");
			if(!file.exists())
			{
				try{
					file.createNewFile();
				} catch(IOException e){
					e.printStackTrace();
				}
			} 		
			else
			{ 
				try{
				FileWriter writer = new FileWriter(file);
				BufferedWriter bw = new BufferedWriter(writer);
				bw.write("\t\t\tFiche client\n");
				bw.write("Numero client : " +clients[indexClientWriter].getId()+"\n");
				bw.write("Nom : " +clients[indexClientWriter].getNom()+"\n");
				bw.write("Prenom : " +clients[indexClientWriter].getPrenom()+"\n");
				bw.write("Date de naissance : " +clients[indexClientWriter].getDateDeNaissance()+"\n");
				bw.write("__________________________________________________________\n");
				System.out.println();
				String compte;
				double solde;
				bw.write("Liste de compte\n");
				bw.write("__________________________________________________________\n");
				bw.write("Numero de compte                       Solde\n");
				bw.write("__________________________________________________________\n");
				for(int i=0; i<comptesBancaires.length; i++){
					if(identifiant.equals(comptesBancaires[i].getIdClient())==true){
						solde = comptesBancaires[i].getSolde();
						compte = comptesBancaires[i].getNumeroCompte();
						bw.write(compte+ "                         " +solde+ "         " + (solde > 0 ? ":)" : ":(") +"\n");
					}
				}
				bw.close();
				writer.close();
				System.out.println();
				} catch(IOException e){
					e.printStackTrace();
				}
			}
		}
		
	}
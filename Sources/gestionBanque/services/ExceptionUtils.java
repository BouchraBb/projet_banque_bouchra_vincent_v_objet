package gestionBanque.services;
import java.util.*;

public class ExceptionUtils {

    public static String checkValue( String value,Scanner sc){
		do{
			if(value.equals("")){
				System.out.println();
				System.out.print("Le champ doit contenir au moins un caractere ! Veuillez recommencer... \n");
				System.out.println();
				value = sc.nextLine();
			}
		} while(value.equals(""));

		return value;
	}
}